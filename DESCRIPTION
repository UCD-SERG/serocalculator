Package: serocalculator
Type: Package
Title: Estimating Infection Rates from Serological Data
Version: 0.1.0.9000
Date: 2022-03-29
Authors@R: c(
  person(given = "Peter", family = "Teunis", email = "p.teunis@emory.edu", role = c("aut", "cph"), comment = "Author of the method and original code."),
  person(given = "Kristina", family = "Lai", role = c("aut")), 
  person(given = "Kristen", family = "Aiemjoy", email = "kaiemjoy@ucdavis.edu", role = c("aut")),
  person(given = "Douglas Ezra", family = "Morrison", email = "demorrison@ucdavis.edu", role = c("aut", "cre")))
Description: Translates antibody levels measured in cross-sectional population 
  samples into estimates of the frequency with which seroconversions (infections) 
  occur in the sampled populations. Replaces the previous `seroincidence` package.
Depends: R (>= 4.2.0)
License: GPL-3
Imports: 
    dplyr (>= 1.1.1),
    ggplot2,
    ggpubr,
    magrittr,
    Rcpp,
    rlang,
    scales,
    stats,
    tibble,
    tidyr,
    utils,
Suggests: 
    parallel,
    knitr,
    rmarkdown,
    pander,
    Hmisc,
    tidyverse,
    fs,
    testthat (>= 3.0.0),
    readr,
    bookdown,
    doParallel,
    printr,
    doRNG,
    rngtools,
    osfr,
    ggbeeswarm,
    DT
VignetteBuilder: knitr
LazyData: true
Encoding: UTF-8
URL: https://github.com/UCD-SERG/serocalculator, https://ucd-serg.github.io/serocalculator/
RoxygenNote: 7.2.3.9000
NeedsCompilation: no
LinkingTo: 
    Rcpp
Language: en-US
Roxygen: list(markdown = TRUE)
Config/testthat/edition: 3
